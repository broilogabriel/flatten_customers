package com.broilogabriel

import scala.annotation.tailrec

package object flatten {

  /**
    * Function to flatten list based on the following rules:
    * Write a function that will flatten an array of arbitrarily nested arrays of integers into a flat array of
    * integers. e.g. [[1,2,[3]],4] → [1,2,3,4]. If the language you're using has a function to flatten arrays, you
    * should pretend it doesn't exist.
    *
    * @param toFlatten List containing any value to flatten
    * @param flattened Argument for the recursive call
    * @return the flattened List of Ints
    */
  @tailrec
  def flatten(toFlatten: List[Any], flattened: List[Int] = List.empty[Int]): List[Int] = {
    if (toFlatten.isEmpty) {
      flattened
    } else {
      if (toFlatten.exists(item =>
        item == null || !(item.isInstanceOf[Int] || item.isInstanceOf[List[Any]])
      )) {
        throw InvalidInputException()
      } else {
        val flatList: Option[List[Int]] = toFlatten.head match {
          case item: Int => Some(List(item))
          case list: List[Any] =>
            val x = if (list.isEmpty || list.exists(!_.isInstanceOf[Int])) None else Some(list.asInstanceOf[List[Int]])
            x
          case _ => None
        }
        val next: List[Any] = if (flatList.isEmpty) {
          toFlatten.head.asInstanceOf[List[Any]] ++ toFlatten.tail
        } else {
          toFlatten.tail
        }
        flatten(next, flattened ++ flatList.getOrElse(List.empty[Int]))
      }
    }
  }

  case class InvalidInputException(private val message: String = "Invalid value in the list",
                                   private val cause: Throwable = None.orNull) extends Exception(message, cause)

}
