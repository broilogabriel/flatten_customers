package com.broilogabriel.model

import math._

object Location {
  val earthRadius = 6378.7D
}

case class Location(latitude: Double, longitude: Double) {
  val latRad: Double = math.toRadians(latitude.toDouble)
  val longRad: Double = math.toRadians(longitude.toDouble)

  def distance(from: Location): Double = {
    Location.earthRadius * acos(
      sin(latRad) * sin(from.latRad) + cos(latRad) * cos(from.latRad) * cos(from.longRad - longRad)
    )
  }

}
