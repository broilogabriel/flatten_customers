package com.broilogabriel.model

import org.json4s.native.JsonMethods
import org.json4s.{DefaultFormats, Formats}


object Customer {
  implicit val formats: Formats = DefaultFormats

  def deserialize(json: String): Customer = {
    val jObj = JsonMethods.parse(json)
    jObj.camelizeKeys.extract[Customer]
  }
}

case class Customer(userId: Long, name: String, latitude: String, longitude: String) {
  val location = Location(latitude.toDouble, longitude.toDouble)
}


