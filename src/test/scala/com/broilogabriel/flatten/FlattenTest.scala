package com.broilogabriel.flatten

import org.scalatest.{FlatSpecLike, Matchers}

class FlattenTest extends FlatSpecLike with Matchers {

  "flatten" should "raise InvalidInputException in case of non Int OR List values" in {
    val thrown = intercept[Exception] {
      flatten(List(1, 2, "A"))
    }
    assert(thrown == InvalidInputException())
  }

  "flatten" should "raise InvalidInputException in case of non Int OR List values in a nested list" in {
    val thrown = intercept[Exception] {
      flatten(List(1, List(18, 3, List("A")), 2))
    }
    assert(thrown == InvalidInputException())
  }

  "flatten" should "raise InvalidInputException in case of null values" in {
    val thrown = intercept[Exception] {
      flatten(List(List(1), 2, null))
    }
    assert(thrown == InvalidInputException())
  }

  "flatten" should "raise InvalidInputException in case of null values in a nested list" in {
    val thrown = intercept[Exception] {
      flatten(List(1, List(18, 3, List(null)), 2))
    }
    assert(thrown == InvalidInputException())
  }

  "flatten" should "flatten the List of Ints with a list in the middle" in {
    val input = List(1, 2, List(3, 4), List())
    val output = flatten(input)
    val expected = List(1, 2, 3, 4)
    assert(output == expected)
  }

  "flatten" should "flatten the List of Ints starting with a list" in {
    val input = List(List(1, 2), 2, List(3, 4), List())
    val output = flatten(input)
    val expected = List(1, 2, 2, 3, 4)
    assert(output == expected)
  }

  "flatten" should "flatten the List of Ints starting with a list and three levels" in {
    val input = List(List(1, 2), 2, List(List(3, 5), 4), List())
    val output = flatten(input)
    val expected = List(1, 2, 2, 3, 5, 4)
    assert(output == expected)
  }

}
