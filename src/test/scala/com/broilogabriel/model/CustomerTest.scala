package com.broilogabriel.model

import org.scalatest.{FlatSpec, Matchers}
import org.json4s.MappingException

class CustomerTest extends FlatSpec with Matchers {


  "Customer.serialize" should "raise Exception" in {
    val thrown = intercept[Exception] {
      Customer.deserialize("{}")
    }
    assert(thrown.isInstanceOf[MappingException])
  }

  "Customer.serialize" should "output a Customer" in {
    val expected = Customer(userId = 12, name = "Christina McArdle", latitude = "52.986375", longitude = "-6.043701")
    val json =
      s"""{"latitude": "${expected.latitude}", "user_id": ${expected.userId}, "name": "${expected.name}", "longitude":
         |"${expected.longitude}"}""".stripMargin
    val result = Customer.deserialize(json)
    assert(expected == result)
  }
}
