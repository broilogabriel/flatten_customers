package com.broilogabriel.model

import org.scalatest.{FlatSpec, Matchers}

class LocationTest extends FlatSpec with Matchers {


  "Location.distance" should "calculate the distance to the same point" in {
    val office = Location(53.339428D, -6.257664D)
    assert(office.distance(office) == 0)
  }

  "Location.distance" should "calculate the distance" in {
    val office = Location(53.339428D, -6.257664D)
    val customer = Customer(userId = 12, name = "Christina McArdle", latitude = "52.986375", longitude = "-6.043701")

    assert(office.distance(customer.location) == 41.819207244155784D)
    assert(customer.location.distance(office) == 41.819207244155784D)
  }

  "Location.distance" should "calculate the distance again" in {
    val office = Location(53.339428D, -6.257664D)
    val location = Location(latitude = 51.92893, longitude = -10.27699)

    assert(office.distance(location) == 313.634235002602)

  }

}
