name := "flatten_customers"

version := "0.1"

scalaVersion := "2.12.3"

coverageEnabled := true

val scalaTestVersion = "3.0.1"
val modules: Seq[ModuleID] = Seq(
  "org.scalactic" %% "scalactic" % scalaTestVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
  "org.json4s" %% "json4s-native" % "3.5.3"
)

libraryDependencies ++= modules


